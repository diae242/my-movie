from fastapi import FastAPI
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.computadora import compu_router
from routers.user import user_router

app = FastAPI()
app.title = "Computadoras FastAPI"
app.version = "0.0.1"

# Middleware para checar si hay errores
app.add_middleware(ErrorHandler)
app.include_router(compu_router)
#app.include_router(user_router)
#app.include_router(movie_router)


# Inicializa la base de datos
Base.metadata.create_all(bind=engine)

