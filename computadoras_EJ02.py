from fastapi import FastAPI, Body
from fastapi.responses import HTMLResponse

app = FastAPI()
app.title = "Venta de Computadoras"
app.version = "1000 pm"

computadoras = [
    {
        "id": 1,
        "marca": "HP",
        "modelo": "Pavilion",
        "color": "Negro",
        "ram": "8GB",
        "almacenamiento": "512GB"
    },
    {
        "id": 2,
        "marca": "Apple",
        "modelo": "MacBook Air",
        "color": "Plata",
        "ram": "16GB",
        "almacenamiento": "256GB"
    },
    {
        "id": 3,
        "marca": "Dell",
        "modelo": "Inspiron",
        "color": "Gris",
        "ram": "12GB",
        "almacenamiento": "1TB"
    },
    {
        "id": 4,
        "marca": "Lenovo",
        "modelo": "IdeaPad",
        "color": "Azul",
        "ram": "4GB",
        "almacenamiento": "256GB"
    },
    {
        "id": 5,
        "marca": "Asus",
        "modelo": "ZenBook",
        "color": "Dorado",
        "ram": "16GB",
        "almacenamiento": "512GB"
    }
]

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1> Hola Guapo </h1>')

@app.get('/computadoras', tags=['computadoras'])
def get_computadoras():
    return computadoras

@app.get('/computadoras/marca/{marca}', tags=['computadoras'])
def get_computadora_by_marca(marca: str):
    for computadora in computadoras:
        if computadora["marca"] == marca:
            return computadora
    return []

@app.get('/computadoras/color/{color}', tags=['computadoras'])
def get_computadora_by_color(color: str):
    computadoras_encontradas = [computadora for computadora in computadoras if computadora["color"] == color]
    return computadoras_encontradas

@app.put('/computadoras/{id}', tags=['computadoras'])
def update_computadora(id: int, marca: str = Body(...), modelo: str = Body(...), color: str = Body(...), ram: str = Body(...), almacenamiento: str = Body(...)):
    for computadora in computadoras:
        if computadora['id'] == id:
            computadora.update({
                "marca": marca,
                "modelo": modelo,
                "color": color,
                "ram": ram,
                "almacenamiento": almacenamiento
            })
            return computadora
    return []

@app.delete('/computadoras/{id}', tags=['computadoras'])
def delete_computadora(id: int):
    global computadoras
    computadoras = [computadora for computadora in computadoras if computadora["id"] != id]
    return computadoras

@app.post('/computadoras', tags=['computadoras'])
def create_computadora(id: int = Body(...), marca: str = Body(...), modelo: str = Body(...), color: str = Body(...), ram: str = Body(...), almacenamiento: str = Body(...)):
    computadoras.append({
        "id": id,
        "marca": marca,
        "modelo": modelo,
        "color": color,
        "ram": ram,
        "almacenamiento": almacenamiento,
    })
    return computadoras
